import Link from 'next/link';
import styles from './CountryCard.module.css';

const CountryCard = ({ country }) => {
  // current time (**in the client user's timezone**)
  let now = new Date();

  // countryTimezoneOffset is the number of **minutes** the country's
  // time is ahead or behind the UTC time
  let countryTimezoneOffset;

  // if the countries timezone is UTC, it means that it's time
  // is equal to the UTC time. Hence countryTimezoneOffset will be 0
  if (country.timezones[0] === 'UTC') // UTC
    countryTimezoneOffset = 0;
  // if countries timezone is UTC-06 it means that it is 6 hours
  // behind UTC time i.e parseInt('-06')*60 = -360 mins
  else if (country.timezones[0].substring(3).length === 3) // UTC-06 UTC+09
    countryTimezoneOffset = parseInt(country.timezones[0].substring(3))*60;
  // if countries timezone is UTC+05:30 it means that it is 5.5 hours
  // ahead UTC time i.e parseInt('+05')*60 + parseInt('+30') = 330 mins
  else // UTC+05:30
    countryTimezoneOffset = parseInt(country.timezones[0].substring(3, 6))*60 + parseInt(country.timezones[0][3]+country.timezones[0].substring(7));
  
  // now.getTimezoneOffset() gives the **negative** of the difference in
  // time between the client user's time and UTC time. This means that if
  // if we were to add now.getTimezoneOffset() to now (which we are doing in
  // line 30 along with countryTimezoneOffset), we will get UTC time. And
  // adding countryTimezoneOffset to UTC will give us the country's time
  let timezoneOffsetDiff = countryTimezoneOffset + now.getTimezoneOffset();
  let country_now = new Date(now.getTime() + timezoneOffsetDiff * 60 * 1000);

  return (
    <div className={styles.country_card}>
      <img src={country.flag} alt={`${country.name}'s Flag`}/>
      <div className={styles.country_card_details}>
        <h3>{country.name}</h3>
        <p>{`Currency: ${country.currencies.map(currency => currency.name).join(', ')}`}</p>
        <p>{`Current date and time: ${country_now.toDateString()}, ${country_now.getHours()}:${`${country_now.getMinutes()}`.length===2 ? country_now.getMinutes() : `0${country_now.getMinutes()}`}`}</p>
        <div className={styles.country_card_details_buttongrp}>
          <a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${country.name.split(' ').join('+')}`}>Show Map</a>
          <Link href={`/${country.alpha3Code}`}>Detail</Link>
        </div>
      </div>
    </div>
  );
}

export default CountryCard;
