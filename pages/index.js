import Head from 'next/head';
import { useState } from 'react';
import styles from '../styles/Home.module.css';

import Header from '../components/Header/Header';
import SearchBar from '../components/SearchBar/SearchBar';
import CountryCardList from '../components/CountryCardList/CountryCardList';

export default function Home({ countries }) {
  const [countryList, setCountryList] = useState(countries);

  // filterCountryList changes countryList by checking if search is a subtring
  // of the countries' name. We will get search from the SearchBar component
  const filterCountryList = (search) => {
    setCountryList(countries.filter(country => country.name.toLowerCase().startsWith(search)));
  };

  console.log(countryList);
  return (
    <div className={styles.container}>
      <Head>
        <title>Country Web App</title>
      </Head>

      <Header text="Countries"/>
      {/* Passing filterCountryList to SearchBar to get access to
      what the user types */}
      <SearchBar filterCountryList={filterCountryList} />
      <CountryCardList countries={countryList}/>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(`https://restcountries.eu/rest/v2/all`);
  const countries = await res.json();

  return {
    props: {
      countries
    }
  };
};