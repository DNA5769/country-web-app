import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Details.module.css';

import Header from '../components/Header/Header';

export default function Details({ country }) {
  console.log(country);
  return (
    <div className={styles.container}>
      <Head>
        <title>{`Country Web App | ${country.name}`}</title>
      </Head>

      <Header text={country.name}/>

      <div className={styles.main_details}>
        <img src={country.flag} alt={`${country.name}'s Flag`}/>
        <div>
          <p>{`Native Name: ${country.nativeName}`}</p>
          <p>{`Capital: ${country.capital}`}</p>
          <p>{`Population: ${country.population}`}</p>
          <p>{`Region: ${country.region}`}</p>
          <p>{`Sub-region: ${country.subregion}`}</p>
          <p>{`Area: ${country.area} Km`}<sup>2</sup></p>
          <p>{`Country Code: +${country.callingCodes.join(', +')}`}</p>
          <p>{`Languages: ${country.languages.map(lang => lang.name).join(', ')}`}</p>
          <p>{`Currencies: ${country.currencies.map(currency => currency.name).join(', ')}`}</p>
          <p>{`Timezones: ${country.timezones.join(', ')}`}</p>
        </div>
      </div>

      {country.borders.length!==0 ?
        <div className={styles.neighborbox}>
          <h3>Neighbour Countries</h3>
          <div className={styles.flagbox}>
            {country.borders.map(border => <Link key={border[0]} href={`/${border[0]}`}><img src={border[1]} alt={`${border[0]}'s Flag`}/></Link>)}
          </div>
        </div>
        : null
      }
    </div>
  );
};

export async function getStaticPaths() {
  const res = await fetch('https://restcountries.eu/rest/v2/all');
  const countries = await res.json();

  // paths will be a list of the alpha3codes of all the countries
  const paths = countries.map(country => ({ params: { code: country.alpha3Code } }));

  return { 
    paths,
    fallback: false
  };
};

export async function getStaticProps({ params }) {
  const res = await fetch(`https://restcountries.eu/rest/v2/all`);
  const countries = await res.json();
  // params.code is the alpha3Code of the country whose details are to be
  // displayed. Hence we find the country who has that alpha3Code from countries
  let country = countries.filter(country => country.alpha3Code === params.code)[0];

  // country.borders only contains the alpha3Codes of the neighboring countries
  // However we want the flag of the country as well to display it. So we search
  // for the flag from countries
  country.borders = country.borders.map(code => [code, countries.filter(country => country.alpha3Code === code)[0].flag]);

  return { 
    props: { 
      country
    } 
  };
}