# Country Web App
A web app which lists all the countries and allows you to view their map and other details as well

Project Live [Here!](https://country-web-app.vercel.app/)